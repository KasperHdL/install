#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

print(){
    echo -e "\033[47;30m\033[47m$1\033[0m"
}

print "You should update using 'sudo pacman -Syu' and reboot"
print
print "Enabling color in pacman.."
sudo sed -i 's/#Color/Color/g' /etc/pacman.conf

print "Changing to SSH"
git remote set-url origin git@gitlab.com:KasperHdL/install.git

#Setup Yay
print "Installing Yay"
sudo pacman -S yay --noconfirm

print "Update"
yay --noconfirm

print "Installing curl and rsync"
yay -S curl rsync --noconfirm

print "Installing Nerd Fonts"
rsync -arv $SCRIPTPATH/fonts $HOME/.local/share/

print "Config Files"
print "- Installing rsync"
print "- Moving to Temp"
mkdir $HOME/temp
cd $HOME/temp

print "- Cloning"
git clone git@gitlab.com:KasperHdL/config.git .config

print "- Merging from Temp into $HOME/.config"
rsync -arv $HOME/temp/.config $HOME/
sudo rm -rf $HOME/temp

print "Installing Custom Keyboard"
sudo cp /usr/share/X11/xkb/symbols/us /usr/share/X11/xkb/symbols/us_backup
sudo cp $HOME/.config/keyboard_layout/us /usr/share/X11/xkb/symbols/us


# FISH
print "installing fish"
yay -S fish --noconfirm

print "installing oh-my-fish"
curl -L https://get.oh-my.fish | fish

print "Installing i3"
yay -S i3-gaps i3blocks-gaps-git i3lock-fancy-git rofi feh kitty sl tree --noconfirm

kitty sl


print "Installing Neovim"
yay -S neovim python-neovim clang mono cmake silver-searcher-git --noconfirm


print "- Installing Vim Plug"
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    
#installs fzf
print "- Installing Plugins(Might take a while)"
kitty nvim +":UpdateRemotePlugins" +":PlugInstall" +":qa"

#install default applications
print "Installing default applications"
yay -S firefox spotify --noconfirm

print "Installing general terminal applications"
yay -S bat xclip --noconfirm

print "Installing development terminal applications"
yay -S zeal python-gdbgui tokei grv sublime-merge --noconfirm

print "- Changing Shell"
chsh -s $(grep /fish$ /etc/shells | tail -1)
