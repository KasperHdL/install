# Install Script

# What you need to do:
* Setup SSH keys

# What it installs
* yay
* installs nerd fonts(in the fonts folder)
* My config
* My custom keyboard (in my config/keyboard\_layot)

## Terminal tools
* fish and oh-my-fish(and theme)
* rsync + curl
* i3-gaps + rofi + feh 
* kitty
* Neovim with VimPlug
* gdbgui
* zeal
* grv + meld
* bat + tokei
* xclip


## Programs
* firefox
* spotify

# To Do:
* 
